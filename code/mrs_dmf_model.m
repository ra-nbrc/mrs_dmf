dtt   = 1e-3;   % Sampling rate of simulated neuronal activity (seconds)
ds   = 100;    % BOLD downsampling rate
%%

% loading DMF model cortical pool parameters

%%%%%%%%%

dt=0.1;
tmax=120000;
tspan=0:dt:tmax;
tmax1=4000;
tspan1=0:dt:tmax1;

taon=100;
taog=10;
gamma=0.641;
sigma=0.001;
JN=0.15;
J=0.9*ones(Nnew,1);
I0=0.382;
Jexte=1.0;
Jexti=0.7;
w=1.4;
%Iext=0.02;
curr=zeros(tmax1,Nnew);
curr_new=zeros(tmax,Nnew);
neuro_act=zeros(tmax,Nnew);
neuro_act2=zeros(tmax1,Nnew);
ix=1;
we=0.5;
%G=0.1:0.025:3.0; %relevant Global scaling parameter to simulate global cortical model
%max_firing_rate = zeros(1,66);%firing rate of all cortical pools Intialized
%for we=G;
%we
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Balanced feedback Inhibiton control commented
%k=1;
% sn=0.001*ones(Nnew,1);
% sg=0.001*ones(Nnew,1);
% Iext=zeros(Nnew,1);
% delta=0.02*ones(Nnew,1);
% 
% for k=1:5000
%  sn=0.001*ones(Nnew,1);
%  sg=0.001*ones(Nnew,1);
%  nn=1;
%  j=0;
% for i=2:1:length(tspan1)
%   xn=I0*Jexte+w*JN*sn+we*JN*C*sn-J.*sg;
%   xg=I0*Jexti+JN*sn-sg;
%   rn=phie(xn);
%   rg=phii(xg);
%   sn=sn+dt*(-sn/taon+(1-sn)*gamma.*rn./1000.)+sqrt(dt)*sigma*randn(Nnew,1);
%   sn(sn>1) = 1;  
%   sn(sn<0) = 0;      
%   sg=sg+dt*(-sg/taog+rg./1000.)+sqrt(dt)*sigma*randn(Nnew,1);
%   sg(sg>1) = 1;        
%   sg(sg<0) = 0;
%   j=j+1;
%   if j==10
%    curr(nn,:)=xn'-125/310;
%    neuro_act2(nn,:)=rn';
%    nn=nn+1;
%    j=0;
%   end
%  end
% 
%  currm=mean(curr(1000:end,:),1);
%  flag=0;
%  for n=1:1:Nnew
%   %if (n==2&3) && abs(currm(n)+0.026)>0.005 
%   if abs(currm(n)+0.026)>0.005
%    if currm(n)<-0.026 
%     J(n)=J(n)-delta(n);
%     delta(n)=delta(n)-0.001;
%     if delta(n)<0.001;
%        delta(n)=0.001;
%     end
%    else 
%     J(n)=J(n)+delta(n);
%    end
%   else
%    flag=flag+1;
%   end
%  end
% 
%  if flag==Nnew
%   break;
%  end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sn=0.001*ones(Nnew,1);
sg=0.001*ones(Nnew,1);
%Iext = [0.0;0.0;0.0;0.01;0.0;0.0;0.0;0.0;0.0;0.0];
alphan = 2;
betan = 1;
alphag = 0.2;
betag = 0.1;
glut_conc = 7.46;
gaba_conc = 1.82;
nn=1;
j=0;
disp('simualting the dynamics...')    
for i=2:1:length(tspan)
    xn=I0*Jexte+w*Jn*sn-Jg*sg;
%     xg=I0*Jexti+JN*sn-Jg*sg;
    rn=phie(xn);
%     rg=phii(xg);
    sn=sn+dt*(alphan*glut_conc*(1-sn)-betan*sn)+sqrt(dt)*sigma*randn(Nnew,1);
    sn(sn>1) = 1; 
    sn(sn<0) = 0;             
%     sg=sg+dt*(alphag*gaba_conc*(1-sg)-betag*sg)+sqrt(dt)*sigma*randn(Nnew,1);
%     sg(sg>1) = 1;        
%     sg(sg<0) = 0;
    j=j+1;
    if j==10
        curr_new(nn,:)=xn'-125/310;   
        neuro_act(nn,:)=sn';
        nn=nn+1;
        j=0;
    end
end
disp('simulation completed and synapctic activity computed')
% computes max excitatory firing rate from Each pool
% to compute bifurcation diagram as a function of Global
% scaling parameter 'G'

%max_firing_rate(ix) = max(rn);
%ix=ix+1;
%end
nn=nn-1;

% 
% %%% BOLD empirical calculation using Hemodynamic model
% 
% %Friston BALLOON MODEL
disp('computing BOLD signals using synapctic activity...')
T = nn*dtt; % Total time in seconds

B = BOLD(T,neuro_act(1:nn,1)'); % B=BOLD activity, bf=Foutrier transform, f=frequency range)
BOLD_act = zeros(length(B),Nnew);
BOLD_act(:,1) = B;  
% 
for nnew=2:Nnew
   B = BOLD(T,neuro_act(1:nn,nnew));
   BOLD_act(:,nnew) = B;
end
disp('BOLD sginals computed')
% 
% 
% % Downsampling and reordering removing the first 500ms
bds_FI=BOLD_act(500:ds:end,:);
% 
% % Global regression
% %[b]=Global_regression(bds');
% 
% % clear bds BOLD_filt
% 
% % BOLD correlation matrix = Simulated Functional Connectivity
Cb_FI  = corrcoef(bds_FI);
cb  = atanh(Cb_FI(Isubdiag)); % Vector containing all the FC values below the diagonal 
%cb  = atanh(Cb(Isubdiag)); % Vector containing all the FC values below the diagonal 
imagesc(Cb_FI);colorbar();set(gca,'Fontsize', 20);
% % 
% % %%%%%%%
% % 
%Coef_restFC    = corrcoef(Cb,FC_emp);
% Coef_rest    = corrcoef(cb,sc)
% % % % %fittcorr(ix)=Coef(2)
%fittcorr_rest(ix)=Coef_restFC(2)
%ix=ix+1
%end
%figure
%plot(wee,fittcorr);
% plot(wee,fittcorr2);
% 
% 
% load Human_66.mat C Order
% figure;
% subplot(2,2,1)
% imagesc(C);
% colorbar;
% subplot(2,2,2)
% imagesc(Cnew);
% colorbar;
% subplot(2,1,2)
% plot(wee,fittcorr);

