reply = input('Clear all variables? Y/N [Y]:','s');
if isempty(reply) || strcmpi(reply,'Y')
    clearvars;
else
    return;
end
