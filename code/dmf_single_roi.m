clear all;
dtt   = 1e-3;   % Sampling rate of simulated neuronal activity (seconds)
ds   = 100;    % BOLD downsampling rate
%%

% loading DMF model cortical pool parameters

%%%%%%%%%
nAreas=1;
dt=0.1;
tmax=120000;
tspan=0:dt:tmax;
tmax1=4000;
tspan1=0:dt:tmax1;

taon=100;
taog=10;
gamma=0.641;
sigma=0.001;
JN=0.15;
J=0.9*ones(nAreas,1);
I0=0.382;
Jexte=1.0;
Jexti=0.7;
w=1.4;
%Iext=0.02;
currn=zeros(tmax,nAreas);
currg=zeros(tmax,nAreas);
frn=zeros(tmax,nAreas);
frg=zeros(tmax,nAreas);
neuro_actn=zeros(tmax,nAreas);
neuro_actg=zeros(tmax,nAreas);
ix=1;
we=0.5;
%G=0.1:0.025:3.0; %relevant Global scaling parameter to simulate global cortical model
%max_firing_rate = zeros(1,66);%firing rate of all cortical pools Intialized
%for we=G;
%we
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Balanced feedback Inhibiton control commented
%k=1;
% sn=0.001*ones(Nnew,1);
% sg=0.001*ones(Nnew,1);
% Iext=zeros(Nnew,1);
% delta=0.02*ones(Nnew,1);
% 
% for k=1:5000
%  sn=0.001*ones(Nnew,1);
%  sg=0.001*ones(Nnew,1);
%  nn=1;
%  j=0;
% for i=2:1:length(tspan1)
%   xn=I0*Jexte+w*JN*sn+we*JN*C*sn-J.*sg;
%   xg=I0*Jexti+JN*sn-sg;
%   rn=phie(xn);
%   rg=phii(xg);
%   sn=sn+dt*(-sn/taon+(1-sn)*gamma.*rn./1000.)+sqrt(dt)*sigma*randn(Nnew,1);
%   sn(sn>1) = 1;  
%   sn(sn<0) = 0;      
%   sg=sg+dt*(-sg/taog+rg./1000.)+sqrt(dt)*sigma*randn(Nnew,1);
%   sg(sg>1) = 1;        
%   sg(sg<0) = 0;
%   j=j+1;
%   if j==10
%    curr(nn,:)=xn'-125/310;
%    neuro_act2(nn,:)=rn';
%    nn=nn+1;
%    j=0;
%   end
%  end
% 
%  currm=mean(curr(1000:end,:),1);
%  flag=0;
%  for n=1:1:Nnew
%   %if (n==2&3) && abs(currm(n)+0.026)>0.005 
%   if abs(currm(n)+0.026)>0.005
%    if currm(n)<-0.026 
%     J(n)=J(n)-delta(n);
%     delta(n)=delta(n)-0.001;
%     if delta(n)<0.001;
%        delta(n)=0.001;
%     end
%    else 
%     J(n)=J(n)+delta(n);
%    end
%   else
%    flag=flag+1;
%   end
%  end
% 
%  if flag==Nnew
%   break;
%  end
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 sn=0.001*ones(nAreas,1);
 sg=0.001*ones(nAreas,1);
%Iext = [0.0;0.0;0.0;0.01;0.0;0.0;0.0;0.0;0.0;0.0];
 
 nn=1;
 j=0;
 disp('simualting the dynamics...')    
 for i=2:1:length(tspan)
     
  xn=I0*Jexte+w*JN*sn-J.*sg;
  xg=I0*Jexti+JN*sn-sg;
  rn=phie(xn);
  rg=phii(xg);
  sn=sn+dt*(-sn/taon+(1-sn)*gamma.*rn./1000.)+sqrt(dt)*sigma*randn(nAreas,1);
  sn(sn>1) = 1; 
  sn(sn<0) = 0;             
  sg=sg+dt*(-sg/taog+rg./1000.)+sqrt(dt)*sigma*randn(nAreas,1);
  sg(sg>1) = 1;        
  sg(sg<0) = 0;
  j=j+1;
  if j==10
   currn(nn,:)=xn';   
   currg(nn,:)=xg';
   frn(nn,:)=rn';
   frg(nn,:)=rg';
   neuro_actn(nn,:)=sn';
   neuro_actg(nn,:)=sg';
   nn=nn+1;
   j=0;
  end
 end
 disp('simulation completed and synapctic activity computed')
% computes max excitatory firing rate from Each pool
% to compute bifurcation diagram as a function of Global
% scaling parameter 'G'

%max_firing_rate(ix) = max(rn);
%ix=ix+1;
%end
nn=nn-1;

% 
% %%% BOLD empirical calculation using Hemodynamic model
% 
% %Friston BALLOON MODEL
disp('computing BOLD signals using synapctic activity...')
T = nn*dtt; % Total time in seconds

B = BOLD(T,neuro_actn(1:nn,1)'); % B=BOLD activity, bf=Foutrier transform, f=frequency range)
BOLD_act = zeros(length(B),nAreas);
BOLD_act(:,1) = B;  
% 
for nnew=2:nAreas
   B = BOLD(T,neuro_actn(1:nn,nnew));
   BOLD_act(:,nnew) = B;
end
disp('BOLD sginals computed')
% 
% 
% % Downsampling and reordering removing the first 500ms
bds_FI=BOLD_act(500:ds:end,:);

figure();
subplot(3,2,1)
plot(neuro_actn(1000:end))
title('sn','FontSize',20)
subplot(3,2,2)
plot(neuro_actg(1000:end))
title('sg','FontSize',20)
% figure();
subplot(3,2,3)
plot(currn(1000:end))
title('xn','FontSize',20)
subplot(3,2,4)
plot(currg(1000:end))
title('xg','FontSize',20)
% figure();
subplot(3,2,5)
plot(frn(1000:end))
title('frn','FontSize',20)
subplot(3,2,6)
plot(frg(1000:end))
title('frg','FontSize',20)